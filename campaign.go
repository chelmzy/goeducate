package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// Campaign : This is the struct for processing the json data from the API
type Campaign []struct {
	ID            int       `json:"id"`
	Name          string    `json:"name"`
	CreatedDate   time.Time `json:"created_date"`
	LaunchDate    time.Time `json:"launch_date"`
	CompletedDate time.Time `json:"completed_date"`
	Results       []struct {
		ID        string    `json:"id"`
		Email     string    `json:"email"`
		FirstName string    `json:"first_name"`
		LastName  string    `json:"last_name"`
		Position  string    `json:"position"`
		Status    string    `json:"status"`
		IP        string    `json:"ip"`
		Latitude  int       `json:"latitude"`
		Longitude int       `json:"longitude"`
		SendDate  time.Time `json:"send_date"`
		Reported  bool      `json:"reported"`
	} `json:"results"`
}

// GetCampaigns : This function calls out to the GoPhish API and returns the campaign results
func GetCampaigns(apiKey string, host string) Campaign {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	_, err := http.Get("https://golang.org/")
	if err != nil {
		fmt.Println(err)
	}

	response, err := http.Get("https://" + host + ":3333/api/campaigns/?api_key=" + apiKey)

	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		os.Exit(1)
	}

	temp, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Could not parse HTTP response!")
		os.Exit(1)
	}

	var campaign Campaign
	json.Unmarshal(temp, &campaign)
	if err != nil {
		fmt.Println("Could not unmarshall json!")
		os.Exit(1)
	}
	return campaign
}

// CheckActive : This function checks if there are active campaigns
func CheckActive(campaigns Campaign) bool {
	var isActive bool
	for _, i := range campaigns {
		if i.CompletedDate == notCompleted {
			isActive = true
		}
	}
	return isActive
}

// CheckFails : This function iterates through the results and determines what actions need to be taken
func CheckFails(campaigns Campaign) []string {
	var failEmailList []string
	for _, i := range campaigns {
		b, err := ioutil.ReadFile("fail.log")
		if err != nil {
			fmt.Println("No fail.log file found!")
		}

		s := string(b)

		for _, u := range i.Results {
			if i.CompletedDate == notCompleted && strings.Contains(s, u.Email) == false {
				switch u.Status {
				case "Submitted Data":
					failEmailList = append(failEmailList, u.Email)
				case "Clicked Link":
					failEmailList = append(failEmailList, u.Email)
				}
			}
		}
	}
	return failEmailList
}

// CheckComplete : This function checks if any campaigns need to be expired
func CheckComplete(campaigns Campaign, apiKey string, host string) []string {
	var endEmailList []string

	for _, i := range campaigns {
		finishDate := "Campaign " + strconv.Itoa(i.ID) + " Finished: " + now.Format("01-02-2006") 
		expiredDate := i.LaunchDate.AddDate(0, 0, +3)
		if now.After(expiredDate) && i.CompletedDate == notCompleted {
			for _, u := range i.Results {
				endEmailList = append(endEmailList, u.Email)
			}
			http.Get("https://" + host + ":3333/api/campaigns/" + strconv.Itoa(i.ID) + "/complete/?api_key=" + apiKey)
			AppendStringToFile("fail.log","=============================")
			AppendStringToFile("fail.log", finishDate)
			AppendStringToFile("fail.log","=============================")
		}
	}

	return endEmailList
}
