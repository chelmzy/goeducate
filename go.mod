module gitlab.com/chelmzy/goeducate

require (
	github.com/Masterminds/goutils v1.1.0 // indirect
	github.com/Masterminds/sprig v2.18.0+incompatible // indirect
	github.com/aokoli/goutils v1.1.0 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/jaytaylor/html2text v0.0.0-20190408195923-01ec452cbe43 // indirect
	github.com/kr/pty v1.1.4 // indirect
	github.com/matcornic/hermes/v2 v2.0.2
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5 // indirect
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092 // indirect
	golang.org/x/sys v0.0.0-20190602015325-4c4f7f33c9ed // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190602112858-2de7f9bf822c // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
