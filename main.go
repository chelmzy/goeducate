package main

import (
	"fmt"
	"os"
	"time"
)

// Some global vairables
var notCompleted = time.Date(0001, 01, 01, 00, 00, 00, 00, time.UTC)
var now = time.Now()

// AppendStringToFile : This function appends string to file from https://siongui.github.io/2017/01/23/go-append-text-string-to-file/
func AppendStringToFile(path, text string) error {
	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(text + "\n")
	if err != nil {
		return err
	}
	return nil
}

func main() {
	// Load configuration file
	config, _ := LoadConfiguration("config.json")
	// Get campaign info
	campaigns := GetCampaigns(config.Gophish.Apikey, config.Gophish.Host)

	// Check for active campaigns
	inactive := CheckActive(campaigns)
	if inactive == false {
		fmt.Println("No active campaigns found!")
		os.Exit(1)
	}

	// Check for fails
	failEmailList := CheckFails(campaigns)
	if len(failEmailList) > 0 {
		SendFails(failEmailList, config)
	}

	// Check for campaigns to complete
	endEmailList := CheckComplete(campaigns, config.Gophish.Apikey, config.Gophish.Host)
	if len(endEmailList) > 0 {
		SendEnd(endEmailList, config)
	}

}
