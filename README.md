## GoEducate
1. Edit config.json for your environment.
2. Edit config.json to include your email template and a picture of the phishing email.
3. Build the go executable.
4. Create cronjob to execute program.
5. Phish users like normal in GoPhish and enjoy instant education follow up!