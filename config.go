package main

import (
	"encoding/json"
	"os"
)

// Config : This is the struct for processing the json config file
type Config struct {
	Gophish struct {
		Apikey string `json:"apikey"`
		Host   string `json:"host"`
	}
	Relay struct {
		Host     string `json:"host"`
		Port     int    `json:"port"`
		User     string `json:"user"`
		Password string `json:"password"`
	}
	Email struct {
		From         string `json:"from"`
		FailTemplate string `json:"failtemplate"`
		EndTemplate  string `json:"endtemplate"`
		ImageOne     string `json:"imageone"`
		ImageTwo     string `json:"imagetwo"`
	}
}

// LoadConfiguration : Loads the configuration from json file
func LoadConfiguration(file string) (Config, error) {
	var config Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		return config, err
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	return config, err
}
