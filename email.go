package main

import (
	"bytes"
	"gopkg.in/gomail.v2"
	"html/template"
	"log"
)

// ParseTemplate : Parses HTML files into email template
func ParseTemplate(templateFileName string, data Config) string {
	t, err := template.ParseFiles(templateFileName)

	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		panic(err)
	}
	body := buf.String()
	return body
}

// SendFails : Sends the failure emails and writes failures to the list
func SendFails(failEmailList []string, config Config) {
	d := gomail.NewDialer(config.Relay.Host, config.Relay.Port, config.Relay.User, config.Relay.Password)
	t, err := d.Dial()
	if err != nil {
		panic(err)
	}

	body := ParseTemplate(config.Email.FailTemplate, config)
	m := gomail.NewMessage()
	for _, r := range failEmailList {
		m.SetHeader("From", config.Email.From)
		m.SetAddressHeader("To", r, "")
		m.Embed(config.Email.ImageOne)
		m.Embed(config.Email.ImageTwo)
		m.SetHeader("Subject", "Phishing Education Failure")
		m.SetBody("text/html", body)

		if err := gomail.Send(t, m); err != nil {
			log.Printf("Could not send email to %q: %v", r, err)
		}
		AppendStringToFile("fail.log", r)
		m.Reset()
	}

}

// SendEnd : Sends the follow up emails to everyone in the current campaign
func SendEnd(endEmailList []string, config Config) {
	d := gomail.NewDialer(config.Relay.Host, config.Relay.Port, config.Relay.User, config.Relay.Password)
	t, err := d.Dial()
	if err != nil {
		panic(err)
	}

	body := ParseTemplate(config.Email.EndTemplate, config)
	m := gomail.NewMessage()
	for _, r := range endEmailList {
		m.SetHeader("From", config.Email.From)
		m.SetAddressHeader("To", r, "")
		m.Embed(config.Email.ImageOne)
		m.Embed(config.Email.ImageTwo)
		m.SetHeader("Subject", "Phishing Education")
		m.SetBody("text/html", body)

		if err := gomail.Send(t, m); err != nil {
			log.Printf("Could not send email to %q: %v", r, err)
		}
		AppendStringToFile("end.log", r)
		m.Reset()
	}
}
